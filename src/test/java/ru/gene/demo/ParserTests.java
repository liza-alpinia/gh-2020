package ru.gene.demo;

import org.biojava.bio.seq.RNATools;
import org.biojava.bio.symbol.IllegalAlphabetException;
import org.biojava.bio.symbol.IllegalSymbolException;
import org.biojava.bio.symbol.SymbolList;
import org.biojava.nbio.alignment.Alignments;
import org.biojava.nbio.alignment.SimpleGapPenalty;
import org.biojava.nbio.alignment.template.GapPenalty;
import org.biojava.nbio.alignment.template.PairwiseSequenceAligner;
import org.biojava.nbio.core.alignment.matrices.SimpleSubstitutionMatrix;
import org.biojava.nbio.core.alignment.matrices.SubstitutionMatrixHelper;
import org.biojava.nbio.core.alignment.template.SequencePair;
import org.biojava.nbio.core.alignment.template.SubstitutionMatrix;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.RNASequence;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.compound.RNACompoundSet;
import org.biojava.nbio.core.sequence.template.AbstractSequence;
import org.biojava.nbio.data.sequence.FastaSequence;
import org.biojava.nbio.data.sequence.SequenceUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@SpringBootTest
public class ParserTests {

    @Test
    public void slm() throws IOException, IllegalSymbolException, IllegalAlphabetException, CompoundNotFoundException {
        // Read FASTA files
        final List<FastaSequence> rna = SequenceUtil.readFasta(Files.newInputStream(Path.of("input/mature.fa")));
//        final List<FastaSequence> dna = SequenceUtil.readFasta(Files.newInputStream(Path.of("input/ncbi_dataset/ncbi_dataset/data/GCF_000001405.39/chr1.fna")));
        final List<FastaSequence> dna = SequenceUtil.readFasta(Files.newInputStream(Path.of("input/ncbi_dataset/ncbi_dataset/data/GCF_000001215.4/chrY.fna")));
        RNASequence target = new DNASequence(dna.get(0).getSequence()).getRNASequence();

        for (FastaSequence fastaSequence : rna) {
            SymbolList rnaSymbols = RNATools.createRNA(fastaSequence.getSequence());
            System.out.println(fastaSequence.getId() + " = " + rnaSymbols.seqString());

            SymbolList complement = RNATools.complement(rnaSymbols);
            System.out.println("Complement sequence: " + complement.seqString());

            RNASequence rnaSequence = new RNASequence(complement.seqString());

            short match = 2, gop = -5, gep = -3; // 2, 5, and 3 are coprime; -2 is the mismatch score
            SimpleSubstitutionMatrix<NucleotideCompound> matrix = new SimpleSubstitutionMatrix<NucleotideCompound>(new RNACompoundSet(), match, (short) -match);

            PairwiseSequenceAligner<AbstractSequence<NucleotideCompound>, NucleotideCompound> smithWaterman =
                    Alignments.getPairwiseAligner(rnaSequence, target, Alignments.PairwiseSequenceAlignerType.LOCAL, new SimpleGapPenalty(gop, gep), matrix);

            SequencePair<AbstractSequence<NucleotideCompound>, NucleotideCompound> pair = smithWaterman.getPair();

            System.out.println("LCS - " + pair.getSize() + "Pair = " + pair.toString(60) + " ...");
        }
    }

    @Test
    public void testComplex() throws Exception {

        short match = 2, gop = -5, gep = -3; // 2, 5, and 3 are coprime; -2 is the mismatch score
        SimpleSubstitutionMatrix<NucleotideCompound> mx = new SimpleSubstitutionMatrix<NucleotideCompound>(new RNACompoundSet(), match, (short) -match);

        RNASequence a = new RNASequence("CGUAU  AUAUCGCGCGCGCGAUAUAUAUAUCU UCUCUAAAAAAA".replaceAll(" ", ""));
        RNASequence b = new RNASequence("GGUAUAUAUAUCGCGCGCACGAU UAUAUAUCUCUCUCUAAAAAAA".replaceAll(" ", ""));

//  mismatches:                             ^              ^
// The two alignments should have the same score. The bottom one is the one the aligner found.

        PairwiseSequenceAligner<RNASequence, NucleotideCompound> aligner =
                Alignments.getPairwiseAligner(a, b, Alignments.PairwiseSequenceAlignerType.GLOBAL, new SimpleGapPenalty(gop, gep), mx);
        SequencePair<RNASequence, NucleotideCompound> pair = aligner.getPair();


        int nMatches = "--CGUAUAUAUCGCGCGCGCGAUAUAUAUAUCU-UCUCUAAAAAAA".length() - 2 - 4;
        double expectedScore = nMatches * match
                - 2 * match // there are two mismatches
                + 3 * gop + 4 * gep; // there are 3 gap opens and either 1 or 4 extensions, depending on the def
        System.out.println(aligner.getScore());
    }

    //    @Test
    public void parse() throws IOException, IllegalSymbolException, IllegalAlphabetException, CompoundNotFoundException {

        // Read FASTA files
        final List<FastaSequence> rna = SequenceUtil.readFasta(Files.newInputStream(Path.of("input/mature.fa")));
//        final List<FastaSequence> dna = SequenceUtil.readFasta(Files.newInputStream(Path.of("input/ncbi_dataset/ncbi_dataset/data/GCF_000001405.39/chr1.fna")));
        final List<FastaSequence> dna = SequenceUtil.readFasta(Files.newInputStream(Path.of("input/ncbi_dataset/ncbi_dataset/data/GCF_000001215.4/chrY.fna")));

        DNASequence dnaSequence = new DNASequence(dna.get(0).getSequence());

        for (FastaSequence fastaSequence : rna) {
            SymbolList rnaSymbols = RNATools.createRNA(fastaSequence.getSequence());
            System.out.println(fastaSequence.getId() + " = " + rnaSymbols.seqString());

            SymbolList complement = RNATools.complement(rnaSymbols);
            System.out.println("Complement sequence: " + complement.seqString());

            RNASequence rnaSequence = new RNASequence(complement.seqString());

            // Configure algorithm parameters
            SubstitutionMatrix<NucleotideCompound> matrix = SubstitutionMatrixHelper.getNuc4_2();

            GapPenalty penalty = new SimpleGapPenalty();

            int gop = 8;
            int extend = 1;
            penalty.setOpenPenalty(gop);
            penalty.setExtensionPenalty(extend);

            // Align
            PairwiseSequenceAligner<AbstractSequence<NucleotideCompound>, NucleotideCompound> smithWaterman = Alignments.getPairwiseAligner(
                    rnaSequence, dnaSequence, Alignments.PairwiseSequenceAlignerType.LOCAL, penalty, matrix
            );

            SequencePair<AbstractSequence<NucleotideCompound>, NucleotideCompound> pair = smithWaterman.getPair();

            System.out.println("Pair = " + pair.toString(60) + " ...");
        }
    }
}
